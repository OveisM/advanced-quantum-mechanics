# Advanced Quantum Mechanics

## Description
This project is a group project as part of the practical work for the advanced course of Quantum Mechanics, in December 2021 to January 2022.

### Project 1: Numerical integration of the Schrödinger equation
The main purpose of this numerical exercise is to study a dynamical quantum system. To do this, we use the Richardson and the split operator methods. We make some comparisons between these two integrators based on the results to find their limitations, as well as their stability. We also explore the STIRAP process that involves the adiabatic evolution of states.

### Project 2: Numerical Representation of Infinite Dimensional Systems
The goal of this practical work is to make a comparison between the *Finite Difference Representation* (FDR) and the Discrete Variable Representation (DVR) for three known quantum systems; Particle in a box, Harmonic Oscillator and $H_2^+$ molecule.x

### Project 3: Wave Packet Propagation
In this practical work we group the two problems viewed before; The integration of the Schrödinger equation and the representation of infinite dimensional systems. Thus, here we have a space partition $\{x_0,...,x_M\}$ of step $\Delta x$ along with a time partition $\{t_0, ..., t_N \}$ of step $\Delta t$.

For this, we consider a dynamical system described by the Hilbert space $\mathcal{H} = L^2(\mathbb{R},dx)$ and the Hamiltonian $t \rightarrow H(t) = − \frac{\hbar^2}{2m} \frac{d^2}{ dx^2}+ V(x,t)$. The initial condition of the wavepacket is $\psi(x,t=0)=\frac{1}{\sqrt{2\pi}}\intop_{-\infty}^{+\infty}\psi_{k}(t=0)e^{ikx}dk$. We look at the Schrödinger equation with this initial condition.

The goal of this practical work is the study of wave packet propagations in different situations;  Wave packet in a box and quantum carpet, Wave packet in front of a potential barrier and the Quantum Fermi accelerator.

## Support
Do not hesitate to contact me if you have any questions, find any mistakes or any suggestions to improve the codes.
Email: oveis.mahmoudi@gmail.com

## Licence
The codes are developed by the following students of the University of Franche-Comté, France: 

"HADI NASSER ELDINI" <hadi.nasser_eldini@edu.univ-fcomte.fr> , Master program in Physics and Computational Physics

"SANJAY SHARMA" <sanjay.sharma@edu.univ-fcomte.fr> , Master program in PICS (Photonics, Micronanotechnology, Time-Frequency Metrology, and Complex Systems)

"OVEIS MAHMOUDI" <oveis.mahmoudi@edu.univ-fcomt.fr> , Master program in Physics and Computational Physics


Université Bourgogne Franche-Comté
